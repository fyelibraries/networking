#ifndef CLIENT_H
#define CLIENT_H

#include "common.h"

#include "socket.h"


struct Client // TODO: Client is a peer, it is a general peer (can overlap name/info, socket etc.?) -> Refactor. Would allow better use of Peer functions (e.g. PeerSendToPeer)
{
    const char* name;
    const char* info;

    struct Peer server;
};


// ---------------------------- HELPERS ---------------------------------------
void PrintClientInfo( struct Client* client );


// ----------------------------------------------------------------------------
void InitializeClient( const char* name, const char* info, struct Client* client );
void CreateClient( const char* host, const char* port, enum SocketType socketType, enum SocketProtocol socketProtocol, 
                   enum AddressFamily addressFamily, int flags, struct Client* client );

//void ClientSendRaw( char* sendBuffer, int sendBufferLength, int flags, struct Client* client ); // TODO: Maybe useful to have send without restriction of Message structure? Have Peer version (instead?)?
void ClientSend( char* sendBuffer, int sendBufferLength, int flags, struct Client* client );
void ClientReceive( char* receiveBuffer, int receiveBufferLength, int flags, struct Client* client );

void DestroyClient( struct Client* client );

#endif // CLIENT_H

