
#ifndef SOCKET_H
#define SOCKET_H

#ifdef _WIN32 // Windows' headers
#include <wsrm.h>
#endif

//#pragma comment(lib, "ws2_32.lib")

typedef unsigned short ADDRESS_FAMILY;

#ifdef _WIN32
    typedef SOCKET SOCKET_DESCRIPTOR;
#define WAIT_QUEUE_MAX_SIZE SOMAXCONN
#endif

#define STATUS_TIMEOUT_SECONDS 1
#define STATUS_TIMEOUT_MICROSECONDS 0


enum SocketType
{
    stNONE
#ifdef _WIN32
    ,
    stSTREAM = SOCK_STREAM,
    stDGRAM = SOCK_DGRAM,
    stRAW = SOCK_RAW,
    stRDM = SOCK_RDM, // Reliable Datagram
    stSEQPACKET = SOCK_SEQPACKET
#endif
};

enum SocketProtocol
{
    spNONE = 0
#ifdef _WIN32
    ,
    spIPPROTOCOL_TCP = IPPROTO_TCP,
    spIPPROTOCOL_UPD = IPPROTO_UDP,
    spIPPROTOCOL_RM = IPPROTO_RM // Reliable multicast
#endif
};

enum AddressFamily
{
    afNONE = 0
#ifdef _WIN32
    ,
    afUNSPECIFIED = AF_UNSPEC,
    afINET = AF_INET,
    afNETBIOS = AF_NETBIOS,
    afINET6 = AF_INET6,
    afIRDA = AF_IRDA,
    afBTH = AF_BTH
#endif
};

enum AddressInfoFlags
{
    aifNone
#ifdef _WIN32
    , // TODO: Need to find appropriate header file and fix environment...
    aifPASSIVE = 0x01, //AI_PASSIVE,
    aifCANONICAL_NAME = 0x02, //AI_CANONNAME,
    aifNUMERIC_HOST = 0x04, //AI_NUMERICHOST,
    aifALL = 0x0100, //AI_ALL,
    aifADDRESS_CONFIG = 0x0400, //AI_ADDRCONFIG,
    aifIPV4_MAPPED = 0x0800, //AI_V4MAPPED,
    aifNON_AUTHORITATIVE = 0x0400, //AI_NON_AUTHORITATIVE,
    aifSECURE = 0x08000, //AI_SECURE,
    aifRETURN_PREFERRED_NAMES = 0x010000, //AI_RETURN_PREFERRED_NAMES,
    aifFQDN = 0x00020000, //AI_FQDN,
    aifFILESERVER = 0x00040000 //AI_FILESERVER
#endif
};

struct InternetAddress //TODO: Refactor to InternetAddress (that's what it means!) -> better yet... IPV4Address (that's what it is)
{
#ifdef _WIN32
    union {
        struct {
            unsigned char byte1;
            unsigned char byte2;
            unsigned char byte3;
            unsigned char byte4;
        } byteByByte;
        struct {
            unsigned short word1;
            unsigned short word2;
        } twoBytesEach;
        unsigned long allFourBytes;
    };
#endif
};

struct SocketAddressInfoRaw
{
#if (_WIN32_WINNT < 0x0600)
    u_short family;
#else 
#ifdef _WIN32
    ADDRESS_FAMILY family;
#endif // _WIN32
#endif
    char data[14]; // TODO: This seems to be what SocketAddressIn has but all bytes crammed together (byte 1 and 2 = Port#? Bytes 3,4,5,6 = IP??? Rest = zero (like zero[8]) Need verify)
                    //              If this is so, then I could just use SocketAddressIn since this struct would be nearly identical to SocketAddressIn and in SocketAddressIn the bytes are already interpreted for me
                    //              InternetAddress (InternetAddress) -> This union is so I can choose how I wish to interpret the same set of 4 bytes!! (GENIUS!), I can do it byte by byte or 2 bytes or all 4 bytes... HAHA
                    //              ITS ALL JUST BYTES IN MEMORY!
};

struct SocketAddressInfo // NOTE: Except for family, all contents are expressed in Network Byte order!
{
#if (_WIN32_WINNT < 0x0600) // Below is in order Windows expects the struct
    short family;
#else
#ifdef _WIN32
    ADDRESS_FAMILY family;
#endif // _WIN32
#endif
    unsigned short port;
#ifdef _WIN32 
    struct InternetAddress internetAddress;
#endif
    char zero[8]; // Zeros probably for function (parameter) compatability with IPV6 address...
};

struct AddressInfo
{
#ifdef _WIN32 // Below is in order Windows expects it
    int flags;
    int family;
    int socketType;
    int protocol;
    size_t addressLength;
    char* canonicalName;
    struct SocketAddressInfo* address; // TODO: ABSTRACT AWAY... Want a pointer?? Is that what msdn has?
    struct AddressInfo* next;
#endif
};


struct Socket // NOTE: "socket" is a function so to avoid confusion and potential of calling variable of this type "socket" which would cause issues if attempt to call socket function, specifiying "Object"
{
    const char* name;
    const char* info;

    struct SocketAddressInfo addressInfo; // TODO: Currently only useful for inbound sockets (which is fine, the host should know its own address (port/ip), but need reinforce and manually populate for host?... How want do?)

    SOCKET_DESCRIPTOR descriptor;
}; 



// ---------------------------- Helpers ---------------------------------------
void PrintSocketInfo( struct Socket* socket );

char* InternetNetworkAddressToString( int addressFamily, struct InternetAddress internetAddress, char* addressOut, int addressOutLength );
char* InternetNetworkPortToString( const unsigned short port, char* portOut, int portOutLength );

// ------------------------- Startup Actions ----------------------------------
void InitializeSocket( const char* name, const char* info, struct Socket* socket );
int CreateSocket( const char* host, const char* port, enum SocketType socketType, enum SocketProtocol socketProtocol, enum AddressFamily addressFamily, int flags, 
                  struct AddressInfo** addressInfoOut, struct Socket* socketObjectOut );
void CreateSocketFromDescriptor( struct Socket* socket, int descriptor, const char* name, const char* info, struct SocketAddressInfo* addressInfo );

// -------------------------- Client Actions ----------------------------------
int ConnectToAddress( struct Socket* socket, struct AddressInfo* addressInfo );

// -------------------------- Server Actions ----------------------------------
void SocketBind( struct Socket* socket, struct AddressInfo* addressInfo );
void SocketListen( struct Socket* socket, int backlog ); // backlog = maximum queue length of pending connections to accept
struct Socket SocketAccept( struct Socket* listeningSocket );

// ----------------------- Transmission Actions -------------------------------
int SocketSend( char* sendBuffer, int sendBufferLength, int flags, struct Socket* connectedSocket );
int SocketStatus( int fileDescriptorCount, fd_set* readFDs, fd_set* writeFDs, fd_set* exceptFDs );
int SocketReceive( char* receiveBuffer, int receiveBufferLength, int flags, struct Socket* connectedSocket );

// ------------------------- Teardown Actions ---------------------------------
void CloseSocketSend( struct Socket* socket );
void CloseSocket( struct Socket* socket );
void DestroySocket( struct Socket* socket );


#endif // SOCKET_H
