#ifndef SERVER_H
#define SERVER_H


#include "socket.h"
#include "common.h"


struct Server // TODO: Server is a peer, it is a general peer (can overlap name/info, socket etc.?) -> Refactor. Would allow better use of Peer functions (e.g. Receive functions for peer?)
{
    const char* name;
    const char* info;

    unsigned char isRunning;

    struct Socket socket;
    struct Peer* clients;
    int clientCount;
};



// --------------------------- CUSTOM HANDLERS ---------------------------------
typedef void(*StartServerHandler)(struct Server*); // Start server handler type for custom handling actions

// ---------------------------- HELPERS ---------------------------------------
void PrintServerInfo( struct Server* server );

// ----------------------------------------------------------------------------
void InitializeServer( const char* name, const char* info, struct Server* server );
void CreateServer( const char* port, enum SocketType socketType, enum SocketProtocol socketProtocol, enum AddressFamily addressFamily, int flags, 
                   struct Server* server );

// TODO: Maybe provide several common handler types for various purposes (e.g. simple repeat data back, multiplayer friendly ones?, HTTP friendly ones?)
void CustomStartServer( StartServerHandler startServerHandler, struct Server* server ); // Generic handler for running server (customizable by user)
void StartServer( struct Server* server ); // Basic handler for running server

void ServerHandleNewConnection( struct Server* server );
void AddClientFromConnectedSocketToServer( struct Socket socket, struct Server* server, const char* welcomeMessage );
void RemoveClientFromServer( struct Peer* client, struct Server* server );

void DestroyServer( struct Server* server );

#endif // SERVER_H

