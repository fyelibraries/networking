
INC=inc
SRC=src
OBJ=obj
LIB=lib

MK_LIB= ar rcs
LIB_NAME= networking.a

COMPILER=gcc

INCLUDE_FLAGS= -I$(INC) -IC:\Users\die20\Documents\Code\Libraries\Logger\inc \
						-IC:\Users\die20\Documents\Code\Libraries\MemoryManager\inc 
LINK_FLAGS= -L C:\Users\die20\Documents\Code\Libraries\Logger\lib
LIBRARIES= -llogger -lWs2_32

CFLAGS= -Wall $(INCLUDE_FLAGS)

SERVER_MAIN=server_main.c
CLIENT_MAIN=client_main.c
PEER_MAIN=peer_main.c


_COMMON_DEPS= socket.h common.h
COMMON_DEPS= $(patsubst %, $(INC)/%, $(_COMMON_DEPS))

_SERVER_DEPS= server.h
SERVER_DEPS= $(patsubst %, $(INC)/%, $(_SERVER_DEPS)) $(COMMON_DEPS)
SERVER_OBJECTS= $(OBJ)/$(subst .c,.o,$(SERVER_MAIN)) $(patsubst $(INC)/%.h, $(OBJ)/%.o, $(SERVER_DEPS))
SERVER_SOURCE= $(SRC)/$(SERVER_MAIN) $(patsubst $(INC)/%.h, $(SRC)/%.c, $(SERVER_DEPS))

_CLIENT_DEPS= client.h
CLIENT_DEPS= $(patsubst %, $(INC)/%, $(_CLIENT_DEPS)) $(COMMON_DEPS)
CLIENT_OBJECTS= $(OBJ)/$(subst .c,.o,$(CLIENT_MAIN)) $(patsubst $(INC)/%.h, $(OBJ)/%.o, $(CLIENT_DEPS))
CLIENT_SOURCE= $(SRC)/$(CLIENT_MAIN) $(patsubst $(INC)/%.h, $(SRC)/%.c, $(CLIENT_DEPS))

_PEER_DEPS= 
PEER_DEPS= $(patsubst %, $(INC)/%, $(_PEER_DEPS)) $(COMMON_DEPS)
PEER_OBJECTS= $(OBJ)/$(subst .c,.o,$(PEER_MAIN)) $(patsubst $(INC)/%.h, $(OBJ)/%.o, $(PEER_DEPS))
PEER_SOURCE= $(SRC)/$(PEER_MAIN) $(patsubst $(INC)/%.h, $(SRC)/%.c, $(PEER_DEPS))

LIBRARY_OBJECTS= $(patsubst %, $(INC)/%, $(_SERVER_DEPS)) $(patsubst %, $(INC)/%, $(_CLIENT_DEPS)) $(patsubst $(INC)/%.h, $(OBJ)/%.o, $(COMMON_DEPS))


SERVER_OUT= server.exe
CLIENT_OUT= client.exe
PEER_OUT= peer.exe



.PHONY: all
all: server client peer


.PHONY: server
server: $(SERVER_OUT)

$(SERVER_OUT): $(SERVER_OBJECTS)
	$(COMPILER) -o $@ $^ $(CFLAGS) $(LINK_FLAGS) $(LIBRARIES)

$(OBJ)/%.o: $(SRC)/%.c $(SERVER_DEPS)
	if not exist "$(OBJ)" mkdir $(OBJ)
	$(COMPILER) -c -o $@ $< $(CFLAGS)


.PHONY: client
client: $(CLIENT_OUT)

$(CLIENT_OUT): $(CLIENT_OBJECTS)
	$(COMPILER) -o $@ $^ $(CFLAGS) $(LINK_FLAGS) $(LIBRARIES)

$(OBJ)/%.o: $(SRC)/%.c $(CLIENT_DEPS)
	if not exist "$(OBJ)" mkdir $(OBJ)
	$(COMPILER) -c -o $@ $< $(CFLAGS)


.PHONY: peer
peer: $(PEER_OUT)

$(PEER_OUT): $(PEER_OBJECTS)
	$(COMPILER) -o $@ $^ $(CFLAGS) $(LINK_FLAGS) $(LIBRARIES)

$(OBJ)/%.o: $(SRC)/%.c $(PEER_DEPS)
	if not exist "$(OBJ)" mkdir $(OBJ)
	$(COMPILER) -c -o $@ $< $(CFLAGS)


library: $(ALL_OBJECTS)
	if not exist "$(LIB)" mkdir $(LIB)
	$(MK_LIB) $(LIB)/lib$(LIB_NAME) $(LIBRARY_OBJECTS)



.PHONY: clean
clean:
ifndef OBJ
	$(error OBJ is not set)
else
	del /q $(OBJ)\*
endif





