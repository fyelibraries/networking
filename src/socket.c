
#ifdef _WIN32 // Windows' headers
#define WIN32_LEAN_AND_MEAN

// NOTE: This chunk of defines must be done before any other header except w32api.h and is for MinGW/GCC Compiler... TODO: might fix some of the definition errors for enum values... (socket.h?)
#include <w32api.h>
// These defines are for MinGW/GCC, there are different one's for other compilers...
#define WINVER WindowsVista
#define _WIN32_WINDOWS WindowsVista
#define _WIN32_WINNT WindowsVista


#include <winsock2.h> // Includes Windows.h already, if needed though, precede with macro: #define WIN32_LEAN_AND_MEAN
#include <ws2tcpip.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "socket.h"
#include "common.h"



// ---------------------------- HELPERS ---------------------------------------
void PrintAddressInfo( struct AddressInfo* addressInfo )
{
    printf("\n");
    LOG_INFO("--------------AddressInfo--------------");
    LOG_INFO("Flags: %i", addressInfo->flags);
    LOG_INFO("Family: %i", addressInfo->family);
    LOG_INFO("SocketType: %i", addressInfo->socketType);
    LOG_INFO("Protocol: %i", addressInfo->protocol);
    LOG_INFO("AddressLength: %lu", (unsigned long) addressInfo->addressLength);
    LOG_INFO("Canonical Name: %s", addressInfo->canonicalName);
    LOG_INFO("SocketAddress - Family: %i", addressInfo->address->family);

    char peer_ipv4_string[INET_ADDRESS_STRING_LENGTH];
    char peer_port_string[INET_PORT_STRING_LENGTH];
    LOG_INFO("SocketAddress - IP:Port = %s:%s", InternetNetworkAddressToString( addressInfo->address->family, addressInfo->address->internetAddress, peer_ipv4_string, INET_ADDRESS_STRING_LENGTH ), 
            InternetNetworkPortToString( addressInfo->address->port, peer_port_string, INET_PORT_STRING_LENGTH ));
    LOG_INFO("--------------    END    --------------");
    printf("\n");
}

void PrintSocketInfo( struct Socket* socket )
{
    LOG_INFO("--------------SocketInfo--------------");
    LOG_INFO("Name: %s", socket->name);
    LOG_INFO("Info: %s", socket->info);
    LOG_INFO("Descriptor: %i\n", (int) socket->descriptor);
    LOG_INFO("--------------    END    --------------");
}




static char* inet_ntop4(const unsigned char *src, char *dst, socklen_t size)
{
	static const char fmt[128] = "%u.%u.%u.%u";
	char tmp[sizeof "255.255.255.255"];
	int l;

    l = sprintf( tmp, fmt, src[0], src[1], src[2], src[3] );
	if (l <= 0 || (socklen_t) l >= size) {
		return (NULL);
	}
	strncpy(dst, tmp, size);
	return (dst);
}

char* InternetNetworkAddressToString( int addressFamily, struct InternetAddress internetAddress, char* addressOut, int addressOutLength )
{
    //if( addressFamily == afINET )
        return (addressOut = inet_ntop4((const unsigned char*) &internetAddress, addressOut, addressOutLength));
}

static char* port_ntop4( const unsigned short port, char* dst, socklen_t size )
{
	char tmp[sizeof "65535"];

    unsigned short converted_port = (unsigned short) ntohl( ((unsigned long) port) << 16 );
    sprintf( tmp, "%u", converted_port );

	strncpy(dst, tmp, size);
	return (dst);
}

char* InternetNetworkPortToString( const unsigned short port, char* portOut, int portOutLength )
{
    //if( addressFamily == afINET )
        return (portOut = port_ntop4(port, portOut, portOutLength));
}

// ---------------------------- SOCKET ---------------------------------------

// TODO: This is misleading, it may be called multiple times but we probably don't want to do WSAStartup every time? (CHECK)
//          Will it be called multiple times or just the host socket? Since we do AcceptSocket and then CreateSocketFromSocketDescriptor (i.e. bypasses initialize...)
void InitializeSocket( const char* name, const char* info, struct Socket* socket )
{
    LOG_CHECKPOINT("InitializeSocket");

    int result;

    socket->name = name;
    socket->info = info;

#ifdef _WIN32
    // For each WSAStartup, there must be a WSACleanup (which only decrements a counter until 0, then actually frees, i.e. no harm in multiple calls, if paired)
    WSADATA wsaData;
    result = WSAStartup( MAKEWORD(2,2), &wsaData );
    if( !result ) LOG_DEBUG("Windows' Winsock using version: %i.%i", LOBYTE( wsaData.wVersion ), HIBYTE( wsaData.wVersion ));

    socket->descriptor = INVALID_SOCKET; // Indicates that socket (DLLs) has at least been initialized
#endif

    if( result )
    {
        LOG_ERROR("InitializeSocket - Failed with error: %i", result);
    }
}

int CreateSocket( const char* host, const char* port, enum SocketType socketType, enum SocketProtocol socketProtocol, enum AddressFamily addressFamily, int flags, 
                  struct AddressInfo** addressInfoOut, struct Socket* socketObjectOut )
{
    LOG_CHECKPOINT("CreateSocket");

    if( socketObjectOut->descriptor != INVALID_SOCKET ) // DLLs haven't been initialized
    {
        LOG_WARNING("CreateSocket - Only accepts initialized and not yet created sockets, socket will be initialized first.");
        char name[256];
        sprintf(name, "(SocketName-%s:%s", host, port);
        InitializeSocket( name, "Remote Socket", socketObjectOut );
    }

    int error;
    struct AddressInfo hints;

    // Define desirables for the address we are after
    memset( &hints, 0, sizeof(hints) );
    hints.family = addressFamily;
    hints.socketType = socketType;
    hints.protocol = socketProtocol;
    hints.flags =  flags;

#ifdef _WIN32
    // Get the address information that is as close to our above desirables (i.e. "hints") as possible
    error = getaddrinfo( host, port, (struct addrinfo*) &hints, (struct addrinfo**) addressInfoOut); // Resolve server address
    if( !error )
    {
        IF_DEBUG(PrintAddressInfo( *addressInfoOut ););
        // Create the socket with whatever settings were actually chosen for our address (as close to our desirables as possible)
        socketObjectOut->descriptor = INVALID_SOCKET; // Window's Socket object, a socket descriptor but not necessarily a file descriptor (unlike in UNIX)
        socketObjectOut->descriptor = socket( (*addressInfoOut)->family, (*addressInfoOut)->socketType, (*addressInfoOut)->protocol );
        if( socketObjectOut->descriptor == INVALID_SOCKET )
        {
            LOG_ERROR("CreateSocket - Invalid Socket!");
            freeaddrinfo( (struct addrinfo*) (*addressInfoOut) );
            // TODO: Teardown things? Call DestroySocket? Socket was never initialized though?
            return -1;
        }
        LOG_DEBUG("Family: %i, SocketType: %i, Protocol: %i\n", (*addressInfoOut)->family, (*addressInfoOut)->socketType, (*addressInfoOut)->protocol );
        LOG_INFO("New Socket Descriptor: %i\n", (int) socketObjectOut->descriptor);

    }
#endif


    // NOTE: If creating application's socket is non-blocking then may get errors (e.g. on client) such as "WouldBlock"=10035
    //       It seems making application's socket blocking is best route (may be wrong)
    unsigned long iMode = 1; // 1 = Non-Blocking, 0 = Blocking
    ioctlsocket( socketObjectOut->descriptor, FIONBIO, &iMode );

    if( error )
    {
        LOG_ERROR("CreateSocket - Failed with error: %i", error);
        // TODO: Teardown things? Call DestroySocket? Socket was never created though...
        return error;
    }

    return 0;
}

void CreateSocketFromDescriptor( struct Socket* socket, int descriptor, const char* name, const char* info, struct SocketAddressInfo* addressInfo )
{
    LOG_CHECKPOINT("CreateSocketFromDescriptor");

    InitializeSocket( name, info, socket );

    // A bad descriptor would be caught at SocketAccept level, so no need to check here
    socket->descriptor = descriptor;

    socket->addressInfo = *addressInfo;

    // TODO: Needed here?
    unsigned long iMode = 1; // 1 = Non-Blocking, 0 = Blocking
    ioctlsocket( socket->descriptor, FIONBIO, &iMode );
}

int ConnectToAddress( struct Socket* socket, struct AddressInfo* addressInfo )
{
    LOG_CHECKPOINT("ConnectToAddress");

    int error;

#ifdef _WIN32
    // NOTE: iMode Workaround of "Would Block" error when Client tries connecting to continue support peer functionality (i.e. still have non-blocking sockets as default)
    u_long iMode = 0; // NON-BLOCKING = 1 (should default to blocking =0)
    ioctlsocket( socket->descriptor, FIONBIO, &iMode );
    error = connect( socket->descriptor, (struct sockaddr*) addressInfo->address, (int) addressInfo->addressLength );
    iMode = 1; // NON-BLOCKING = 1 (should default to blocking =0)
    ioctlsocket( socket->descriptor, FIONBIO, &iMode );

    // If error, then keep trying other addresses in linked list until succeed or no more
    while( error && (addressInfo = addressInfo->next) ) error = connect( socket->descriptor, (struct sockaddr*) addressInfo->address, (int) addressInfo->addressLength );
#endif

    if( error )
    {
        LOG_ERROR("ConnectToAddress - Failed with error: %i", error);
        LOG_ERROR("WSA Last Error: %i\n", WSAGetLastError()); // TODO: Find better place for this
        freeaddrinfo( (struct addrinfo*) addressInfo ); // Free resources from getaddrinfo, not needed once socket is connected
        // TODO: Teardown things? Call DestroySocket?
        return error;
    }
    else
        socket->addressInfo = *(addressInfo->address);

    freeaddrinfo( (struct addrinfo*) addressInfo ); // Free resources from getaddrinfo, not needed once socket is connected

    return 0;
}

void SocketBind( struct Socket* socket, struct AddressInfo* addressInfo )
{
    LOG_CHECKPOINT("SocketBind");

    int error;

#ifdef _WIN32
    error = bind( socket->descriptor, (struct sockaddr*) addressInfo->address, addressInfo->addressLength );

    socket->addressInfo = *addressInfo->address;
    freeaddrinfo( (struct addrinfo*) addressInfo ); // Free resources from getaddrinfo, not needed once socket is connected
#endif

    if( error )
    {
        LOG_ERROR("BindSocket - Failed with error: %i", error);
        LOG_ERROR("WSA Last Error: %i\n", WSAGetLastError()); // TODO: Find better place for this
        // TODO: Teardown things? Call DestroySocket?
    }

}

void SocketListen( struct Socket* socket, int backlog ) // backlog = maximum queue length of pending connections to accept
{
    LOG_CHECKPOINT("SocketListen");

    int error;

#ifdef _WIN32
    if( (error = listen(socket->descriptor, backlog)) == SOCKET_ERROR )
    {
        LOG_ERROR("SocketListen - Failed to listen on IP/Port!");
        LOG_ERROR("WSA Last Error: %i\n", WSAGetLastError()); // TODO: Find better place for this
        // TODO: return after errors? TODO AND DO OTHER TEARDOWN THIGNS, e.g. closesockt WSACLEANUP
    }
#endif

}

struct Socket SocketAccept( struct Socket* listeningSocket )
{
    LOG_CHECKPOINT("SocketAccept");

    // The socket that contains the actual connection with requested (and now accepted) client
    struct Socket connectedSocket = {.name = "ConnectedSocket", .info = "Connected to client"};

#ifdef _WIN32
    connectedSocket.descriptor = INVALID_SOCKET;
    struct SocketAddressInfo client_address;
    memset( &client_address, 0, sizeof(client_address) );
    int client_length = sizeof(client_address); // Contains initial length but after accept() it will contain ACTUAL length of address returned
    if( (connectedSocket.descriptor = accept(listeningSocket->descriptor, (struct sockaddr*) &client_address, &client_length)) == INVALID_SOCKET )
    {
        LOG_ERROR("SocketAccept - Failed to complete connection for inbound request on listening socket %i!", (int) listeningSocket->descriptor);
        LOG_ERROR("WSA Last Error: %i\n", WSAGetLastError()); // TODO: Find better place for this
        // TODO: return after errors? TODO AND DO OTHER TEARDOWN THIGNS, e.g. closesockt WSACLEANUP
    }
    else
    {
        LOG_INFO("Connected on socket %i", (int) connectedSocket.descriptor);
        connectedSocket.addressInfo = client_address;

        // NOTE: In order for select() to function properly (calling recv only when applicable) need connected sockets non-blocking and select() timeout = null
        u_long iMode = 1; // NON-BLOCKING = 1 (should default to blocking =0)
        ioctlsocket( connectedSocket.descriptor, FIONBIO, &iMode );
    }
#endif

    return connectedSocket;
}

int SocketSend( char* sendBuffer, int sendBufferLength, int flags, struct Socket* connectedSocket )
{
    LOG_CHECKPOINT("SocketSend");

    int bytesSent;

#ifdef _WIN32
    if( (bytesSent = send(connectedSocket->descriptor, sendBuffer, sendBufferLength, flags)) == SOCKET_ERROR )
    {
        LOG_ERROR("SocketSend - Failed to send Message!");
        // TODO: Teardown...?
    }
#endif

    LOG_DEBUG("Bytes Sent: %i (requested %i)", bytesSent, sendBufferLength);
    return bytesSent;
}

int SocketStatus( int fileDescriptorCount, fd_set* readFDs, fd_set* writeFDs, fd_set* exceptFDs )
{
    struct timeval timeout = {STATUS_TIMEOUT_SECONDS, STATUS_TIMEOUT_MICROSECONDS};
    // Blocks until timeout only, so can do other things if need be once timeout (asynchronous). If timeout=NULL, then blocks
    return select( fileDescriptorCount, readFDs, writeFDs, exceptFDs, &timeout );
}

int SocketReceive( char* receiveBuffer, int receiveBufferLength, int flags, struct Socket* connectedSocket )
{
    LOG_CHECKPOINT("SocketReceive");

    if( connectedSocket->descriptor == INVALID_SOCKET )
    {
        LOG_WARNING("SocketReceive - Socket (name: '%s', descriptor: %i) is INVALID", connectedSocket->name, (int) connectedSocket->descriptor); 
        return 0; // Close connection
    }

    //LOG_CHECKPOINT("SocketReceive"); // Not used, or else will cause lots of output

    int bytesReceived = 0;

#ifdef _WIN32
    // TODO: This blocks now! Meaning if multiple clients, they won't get served immediately (due to blocking sockets..., but non-blocking has tons of output)
    bytesReceived = recv(connectedSocket->descriptor, receiveBuffer, receiveBufferLength, flags);
    if( bytesReceived == SOCKET_ERROR )
    {
        // NOTE: Currently commented out logging due to experimenting with non-blocking sockets, and therefore calling recv will throw non-blocking error, could check WSAGetLastError == 10035 (WOULDBLOCK) and log all others?
        //              THIS SHOULD OF NOT EVEN BEEN CALLED SINCE SELECT() SHOULDN'T EVEN MARK THIS SOCKET AS AVAILABLE...! ??? PROPERLY FUNCTIONING SELECT() SHOULD FIX THIS...
        //              Below condiitonal is temporary workaround until implement something more long-term
        int error;
        if( (error = WSAGetLastError()) == 10035 )
            bytesReceived = -2;
        else if( error == WSAECONNRESET )
        {
            LOG_WARNING("SocketReceive - Socket (name: '%s', descriptor: %i) lost connection (WSAECONNRESET). Disconnecting", connectedSocket->name, (int) connectedSocket->descriptor);
            return 0; // Close connection
        }
        else
        {
            LOG_ERROR("SocketReceive - Failed to receive Message!");
            LOG_ERROR("WSA Last Error: %i\n", error);//WSAGetLastError()); // TODO: Find better place for this
            // TODO: Teardown...
        }
    }
#endif

    if( bytesReceived ) LOG_DEBUG("Bytes Received: %i (allocated %i)", bytesReceived, receiveBufferLength); // else connection closed
    return bytesReceived;
}


void CloseSocketSend( struct Socket* socket )
{
    LOG_CHECKPOINT("CloseSocketSend");

    int error;

#ifdef _WIN32
    if( (error = shutdown(socket->descriptor, SD_SEND)) )
    {
        LOG_ERROR("CloseSocketSend - Failed to close the SEND side of the socket with error: %i", error);
        // TODO: Teardown...?
    }
#endif
}

// Simply calls destroy socket, provides a more consistent interface with typical socket commands, 
//      but if you "Create..." then you should "Destroy..." i.e. NOT "Close"... never "Open"
void CloseSocket( struct Socket* socket )
{
    LOG_CHECKPOINT("CloseSocket");
    DestroySocket( socket );
}

void DestroySocket( struct Socket* socket )
{
    LOG_CHECKPOINT("DestroySocket");

#ifdef _WIN32
    if( socket->descriptor != INVALID_SOCKET )
    {
        // TODO TODO TODO: Set descriptor to 0???? or something to indicate done? and then check if this is != 0 before closing? or how behave if descriptor = 0 when call closesocket?
        // Should be called only when done receiving data (if client), can shutdown(..., SD_SEND) to shutdown send side of socket and allow client to finish receiving data, then closesocket()
        // closesocket initiates shutdown sequence implicitly if it hasn't been done already (common pattern is to just closesocket, perhaps unless expect need to handle data first?)
        LOG_INFO("Socket Close Error for socket %i? = %i\n", (int) socket->descriptor, closesocket( socket->descriptor ));
        socket->descriptor = INVALID_SOCKET;
    }

    WSACleanup(); // Must have a matching WSAStartup(...) call
#endif


}














