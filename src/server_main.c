
#include <winsock2.h> // Includes Windows.h already, if needed though, precede with macro: #define WIN32_LEAN_AND_MEAN
#include <ws2tcpip.h>

#include <stdio.h>

#ifndef FULL_LOGGING 
#define FULL_LOGGING
#endif
#include "logger.h"

#include "socket.h"
#include "server.h"

int main()
{
    CREATE_FULL_LOGGING(llPRESET_PARANOID, lePRESET_VERY_GRANULAR, lvPRESET_ALL);
    //CREATE_FULL_LOGGING(llPRESET_CAUTIOUS, lePRESET_VERY_GRANULAR, lvPRESET_ALL);
    //CREATE_FULL_LOGGING(llINFO, lePRESET_GRANULAR, lvPRESET_ALL);
    
    struct Server server;
    InitializeServer( "Server", "Serves things", &server );

    char port[6];
    printf("Enter in port number to serve on: "); scanf("%s", port);
    CreateServer( port, stSTREAM, spIPPROTOCOL_TCP, afINET, aifPASSIVE, &server );
    // TODO: BIG BUG: 2 clients connected to server, one disconnets... FLOOD OF ERRORS FROM StartServer error 10038

    CustomStartServer( &StartServer, &server );

    DestroyServer( &server );

    DESTROY_FULL_LOGGING();
    return 0;
}


