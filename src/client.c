
#ifdef _WIN32 // Windows' headers
#include <winsock2.h> // Includes Windows.h already, if needed though, precede with macro: #define WIN32_LEAN_AND_MEAN
#include <ws2tcpip.h>
#endif

#include "stdio.h"

#include "client.h"

// ---------------------------- HELPERS ---------------------------------------
void PrintClientInfo( struct Client* client )
{
    LOG_INFO("--------------ClientInfo--------------");
    LOG_INFO("Name: %s", client->name);
    LOG_INFO("Info: %s", client->info);
    PrintPeerInfo( &client->server );
    LOG_INFO("--------------    END    --------------");
}






// ---------------------------------------------------------------------------------




void InitializeClient( const char* name, const char* info, struct Client* client )
{
    LOG_CHECKPOINT("InitializeClient");

    client->name = name;
    client->info = info;

    // Prepare to connect to Server
    InitializePeer( "Server", "I serve the horde!", &client->server );
}

void CreateClient( const char* host, const char* port, enum SocketType socketType, enum SocketProtocol socketProtocol, enum AddressFamily addressFamily, int flags, 
                   struct Client* client )
{
    LOG_CHECKPOINT("CreateClient");

    // Connect to Server
    CreatePeer( host, port, socketType, socketProtocol, addressFamily, flags, &client->server );

    client->server.name = host;
    LOG_INFO("Connected to %s...", client->server.name);
}

void ClientSend( char* sendBuffer, int sendBufferLength, int flags, struct Client* client )
{
    LOG_CHECKPOINT("ClientSend");

    CreateAndEnqueueMessage( client->name, strlen(client->name), sendBuffer, sendBufferLength, &client->server.sendQueue );
    SendMessageToPeer( &client->server );
}

void ClientReceive( char* receiveBuffer, int receiveBufferLength, int flags, struct Client* client )
{
    LOG_CHECKPOINT("ClientReceive");

    ReceiveMessageFromPeer( &client->server, &DefaultReceiveMessageHandler );
}

void DestroyClient( struct Client* client )
{
    LOG_CHECKPOINT("DestroyClient");

    LOG_INFO("Shutting down client: %s", client->name);

    DestroyPeer( &client->server );

}

