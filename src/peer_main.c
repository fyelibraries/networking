

#include <winsock2.h> // Includes Windows.h already, if needed though, precede with macro: #define WIN32_LEAN_AND_MEAN
#include <ws2tcpip.h>

#include <stdio.h>

#ifndef FULL_LOGGING
#define FULL_LOGGING
#endif
#include "common.h"

#include "socket.h"
#include "client.h"



enum SendScope // TODO: Broadcast essentiallya  type of multicast.... do I want this enum? 
                        // Point to point would require  specification of other point anyways (can deduce point to point)
{
    POINT_TO_POINT,
    BROADCAST,
    MULTICAST
};

int main()
{
    //CREATE_FULL_LOGGING(llINFO, lePRESET_VERY_GRANULAR, lvPRESET_ALL);
    //CREATE_FULL_LOGGING(llINFO, lePRESET_GRANULAR, lvPRESET_ALL);
    CREATE_FULL_LOGGING(llPRESET_PARANOID, lePRESET_VERY_GRANULAR, lvPRESET_ALL);
    //CREATE_FULL_LOGGING(llPRESET_CURIOUS, lePRESET_VERY_GRANULAR, lvPRESET_ALL);

    struct Peer peer;
    InitializePeer( "Peer", "Count me on-board!", &peer );
    
    char port[6];
    printf("Enter in port number for this peer: "); scanf("%s", port);
    CreatePeer( NULL, port, stSTREAM, spIPPROTOCOL_TCP, afINET, aifPASSIVE, &peer ); // Only create listening end

    PeerListen( &peer );

    char myChar = 'y';
    do
    {
        CustomHandlePeerEvents( &HandlePeerEvents, &peer );

        if( myChar == 'p' ) // Connect to new peer
        {
            char port[6];
            printf("Enter in port number to connect to: "); scanf("%s", port);
            ConnectPeerToNewPeer( &peer, "localhost", port, stSTREAM, 
                                  spIPPROTOCOL_TCP, afINET, aifCANONICAL_NAME ); // Only create listening end... later... what?
        }
        else if( myChar == 's' ) // Send message
        {
            char message[100];
            printf("Enter in message!: "); fflush(stdin); fgets(message, 100, stdin);
            message[strlen(message)] = '\0';
            
            int MESSAGE_TYPE = BROADCAST;
            switch( MESSAGE_TYPE )
            {
                case BROADCAST:
                {
                    PeerSendToPeers( &peer, message, 0, NULL, 0 );
                } break;

                case POINT_TO_POINT:
                {
                } break;

                default:
                {
                } break;
            }
            fflush(stdin);
        }
    } while( (myChar = getchar()) != 'n');

    DestroyPeer( &peer );
    WSACleanup();

    DESTROY_FULL_LOGGING();
    return 0;
}

