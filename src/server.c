
#ifdef _WIN32 // Windows' headers
#include <winsock2.h> // Includes Windows.h already, if needed though, precede with macro: #define WIN32_LEAN_AND_MEAN
#include <ws2tcpip.h>
#endif

#include <stdio.h>

#include "server.h"

// ---------------------------- HELPERS ---------------------------------------
void PrintServerInfo( struct Server* server )
{
    LOG_INFO("--------------ServerInfo--------------");
    LOG_INFO("Name: %s", server->name);
    LOG_INFO("Info: %s", server->info);
    PrintSocketInfo( &server->socket );
    for( int clientCounter = 0; clientCounter < server->clientCount; ++clientCounter )
        PrintPeerInfo( &server->clients[clientCounter] );
    LOG_INFO("--------------    END    --------------");
}






// ---------------------------------------------------------------------------------




void InitializeServer( const char* name, const char* info, struct Server* server )
{
    LOG_CHECKPOINT("InitializeServer");

    server->name = name;
    server->info = info;

    server->isRunning = 0;

    InitializeSocket( "ServerSocket", "Socket server uses", &server->socket );

    server->clients = 0;
    server->clientCount = 0;
}

void CreateServer( const char* port, enum SocketType socketType, enum SocketProtocol socketProtocol, enum AddressFamily addressFamily, int flags, 
                   struct Server* server )
{
    LOG_CHECKPOINT("CreateServer");

    //CreatePeer( NULL, port, socketType, socketProtocol, addressFamily, flags, /*TODO server */ ); // TODO: Can utilize this single method (not any below) if can turn server into a Peer...

    struct AddressInfo* addressInfo;
    CreateSocket( NULL, port, socketType, socketProtocol, addressFamily, flags, &addressInfo, &server->socket );
    SocketBind( &server->socket, addressInfo );
}

void BuildServerFileDescriptorSets( struct Server* server, fd_set* readFDs, fd_set* writeFDs, fd_set* exceptFDs )
{
    FD_ZERO( readFDs );
    FD_SET( server->socket.descriptor, readFDs );

    for( int clientCounter = 0; clientCounter < server->clientCount; ++clientCounter )
    {
        FD_SET( server->clients[clientCounter].connectedSocket.descriptor, readFDs );
    }


    FD_ZERO( writeFDs );
    for( int clientCounter = 0; clientCounter < server->clientCount; ++clientCounter )
    {
        // If SERVER has data to send to this particular peer (i.e. "I have bytes that I am currently sending to peer")
        if( server->clients[clientCounter].currentSendingByte > 0 )
            FD_SET( server->clients[clientCounter].connectedSocket.descriptor, writeFDs );
    }

    FD_ZERO( exceptFDs );
    for( int clientCounter = 0; clientCounter < server->clientCount; ++clientCounter )
    {
        FD_SET( server->clients[clientCounter].connectedSocket.descriptor, exceptFDs );
    }
}

void ServerHandleNewConnection( struct Server* server )
{
    LOG_CHECKPOINT("ServerHandleNewConnection");

    AddClientFromConnectedSocketToServer( SocketAccept( &server->socket ), server, "WELCOME!" );
}

void CustomStartServer( StartServerHandler startServerHandler, struct Server* server )
{
    LOG_CHECKPOINT("CustomStartServer");

    (*startServerHandler)(server);
}

void StartServer( struct Server* server )
{
    LOG_CHECKPOINT("StartServer");
    server->isRunning = 1;

    SocketListen( &server->socket, WAIT_QUEUE_MAX_SIZE );
    char peer_port_string[INET_PORT_STRING_LENGTH];
    LOG_INFO("Server '%s' now listening on port %s...", server->name, 
             InternetNetworkPortToString(server->socket.addressInfo.port, peer_port_string, INET_PORT_STRING_LENGTH));

    fd_set read_fds;
    fd_set write_fds;
    fd_set except_fds;


    //u_long iMode = 0; // NOTE NON-BLOCKING = 1 (should default to blocking =0)
    //ioctlsocket( server->socket.descriptor, FIONBIO, &iMode );


    int count;
    //struct timeval timeout = {3, 0}; // TODO: Make constants #defines? e.g. RECV_DELAY_SEC, RECV_DELAY_USEC
    while( server->isRunning )
    {
        BuildServerFileDescriptorSets( server, &read_fds, &write_fds, &except_fds );

        count = select( 0, &read_fds, &write_fds, &except_fds, 0 ); // Blocks until timeout only, so can do other things if need be once timeout
        if( count > 0 )
        {
            if( FD_ISSET( server->socket.descriptor, &read_fds ) )
            {
                ServerHandleNewConnection( server );
            }

            for( int clientCounter = 0; clientCounter < server->clientCount; ++clientCounter )
            {
                if( FD_ISSET( server->clients[clientCounter].connectedSocket.descriptor, &read_fds ) )
                {
                    if( ReceiveMessageFromPeer( &server->clients[clientCounter], &DefaultReceiveMessageHandler ) != 0 )
                    {
                        RemoveClientFromServer( &server->clients[clientCounter], server );
                        clientCounter--;
                        continue;
                    }
                    //LOG_INFO("Received: %s", server->clients[clientCounter].receivingMessage.data);
                }

                if( FD_ISSET( server->clients[clientCounter].connectedSocket.descriptor, &write_fds ) )
                {
                    // TODO: Expand current SendMessageToPeer functionality (which is WELCOME after connect), to be able to send messages back and forth (text in console?)
                    /*if( SendMessageToPeer( &server->clients[clientCounter], &SOME_HANDLER_FUNCTION ) != 0 )
                    {
                        //TODO: CloseClientConnection(...)
                        continue;
                    }*/
                }

                if( FD_ISSET( server->clients[clientCounter].connectedSocket.descriptor, &except_fds ) )
                {
                    LOG_ERROR("StartServer - Exception for client '%s'... Removing!", server->clients[clientCounter].name);
                    RemoveClientFromServer( &server->clients[clientCounter], server );
                    continue;
                }
            }

            if( FD_ISSET( server->socket.descriptor, &except_fds ) )
            {
                LOG_ERROR("StartServer - Exception for server '%s'... Destroying!", server->name);
                DestroyServer( server );
            }

        }
        else if( count == SOCKET_ERROR ) // SOCKET_ERROR = -1
        {
            LOG_ERROR("StartServer - Failed during updating socket sets");
            LOG_ERROR("WSA Last Error: %i\n", WSAGetLastError()); // TODO: Find better place for this, or a better wrapper so can be used generally... ("Last Error: %i", GetLastError())?
        }
        else
        {
            // SHOULD NEVER OCCUR
            LOG_ERROR("StartServer - Received unacceptable return value from select()");
        }
    }
}

void AddClientFromConnectedSocketToServer( struct Socket socket, struct Server* server, const char* welcomeMessage )
{
    LOG_CHECKPOINT("AddClientFromConnectedSocketToServer");

    char client_name[MAX_NAME_LENGTH];
    sprintf( client_name, "Client-%i", server->clientCount );
    char client_info[MAX_INFO_LENGTH];
    sprintf( client_info, "Client-%i belongs to server '%s'", server->clientCount, server->name );

    server->clients = realloc( server->clients, sizeof(struct Peer) * (server->clientCount+1) );
    CreatePeerFromConnectedSocket( &server->clients[server->clientCount++], &socket, client_name, client_info );

    CreateAndEnqueueMessage( server->name, strlen(server->name), welcomeMessage, strlen(welcomeMessage), &server->clients[server->clientCount-1].sendQueue );
    SendMessageToPeer( &server->clients[server->clientCount-1] );
}

void RemoveClientFromServer( struct Peer* client, struct Server* server )
{
    LOG_CHECKPOINT("RemoveClientFromServer");

    SOCKET socketToRemove = client->connectedSocket.descriptor;

    for( int clientCounter = 0; clientCounter < server->clientCount; ++clientCounter ) // TODO: Optimize with proper search algorithm (e.g. sort all clients by SOCKET # when add?)
                                                                                        //       Also, consider not doing dynamic memory...? Therefore removing would just be setting socket descriptor == NO_SOCKET
    {
        if( server->clients[clientCounter].connectedSocket.descriptor == socketToRemove )
        {
            PeerSwap( &server->clients[clientCounter], &server->clients[--server->clientCount] );
            DestroyPeer( &server->clients[server->clientCount] );
            server->clients = realloc( server->clients, sizeof(struct Peer) * server->clientCount );
        }
    }
}


void DestroyServer( struct Server* server )
{
    LOG_CHECKPOINT("DestroyServer");

    LOG_INFO("Shutting down server: %s", server->name);

    server->isRunning = 0;

    for( int clientCounter = 0; clientCounter < server->clientCount; ++clientCounter )
        DestroyPeer( &server->clients[clientCounter] );

    free( server->clients );
    server->clientCount = 0;

    CloseSocket( &server->socket );
}


