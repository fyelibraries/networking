
#ifdef _WIN32 // Windows' headers
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h> // Includes Windows.h already, if needed though, precede with macro: #define WIN32_LEAN_AND_MEAN
#include <ws2tcpip.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "socket.h"

#include "common.h"




// ------------------------------ HELPERS --------------------------------------
void PrintPeerInfo( struct Peer* peer )
{
    LOG_INFO("--------------PeerInfo--------------");
    LOG_INFO("Name: %s", peer->name);
    LOG_INFO("Info: %s", peer->info);
    // TODO: PrintMessageQueue(); // Print messages in queue
    // TODO: PrintSendingMessages(); // Print current sending byte? Or all? perhaps both?
    // TODO: PrintReceivingMessages(); // Print current receiving byte? Or all? perhaps both?
    PrintSocketInfo( &peer->connectedSocket );
    LOG_INFO("--------------    END    --------------");
}



// ---------------------------- MESSAGE QUEUE ----------------------------------

void CreateMessageQueue( int maxQueueSize, struct MessageQueue* queue )
{
    LOG_CHECKPOINT("CreateMessageQueue");

    VALIDATE_OBJECT( queue->messages = calloc( maxQueueSize, sizeof(struct Message) ), "CreateMessageQueue - " );
    queue->maxSize = maxQueueSize;

    queue->currentMessageIndex = -1;
}

void CreateAndEnqueueMessage( const char* sender, int senderLength, const char* data, int dataLength, struct MessageQueue* queue )
{
    LOG_CHECKPOINT("CreateAndEnqueueMessage");
    if( senderLength > SENDER_MAXSIZE || dataLength > DATA_MAXSIZE )
    {
        LOG_ERROR("The requested sender length of %i (max: %i) or the data length of %i (max: %i) exceeds the max", senderLength, SENDER_MAXSIZE, dataLength, DATA_MAXSIZE );
    }

    struct Message message;
    sprintf(message.sender, sender);
    sprintf(message.data, data);

    EnqueueMessage( queue, &message );
}

void EnqueueMessage( struct MessageQueue* queue, struct Message* message )
{
    LOG_CHECKPOINT("EnqueueMessage");

    if( queue->currentMessageIndex+1 >= queue->maxSize )
    {
        LOG_ERROR("Breached queue compacity of %i, not queueing!", queue->maxSize);
        return;
    }

    queue->messages[++queue->currentMessageIndex] = *message;
}

int DequeueMessage( struct MessageQueue* queue, struct Message* messageOut )
{
    LOG_CHECKPOINT("DequeueMessage");

    if( queue->currentMessageIndex < 0 )
    {
        LOG_DEBUG("No message in queue.");
        return -1;
    }

    *messageOut = queue->messages[queue->currentMessageIndex];
    memset( &queue->messages[queue->currentMessageIndex--], 0, sizeof(struct Message) );

    return 0;
}

void DestroyMessageQueue( struct MessageQueue* queue )
{
    LOG_CHECKPOINT("DestroyMessageQueue");

    FREE_OBJECT( queue->messages );
    queue->maxSize = 0;
    queue->currentMessageIndex = -1;
}

// --------------------------------- Mesh -------------------------------------- 
void InitializeMesh( const char* id, struct Mesh* mesh )
{
    mesh->id = id;
}

void CreateMesh()
{
}

void InitializeAndCreateMesh( const char* id, struct Mesh* mesh) 
{
    InitializeMesh( id, mesh );
    CreateMesh();
}

// ------------------------------- Resolver ------------------------------------ 
void InitializeResolver( const char* name, const char* info, struct Resolver* resolver )
{
    resolver->name = name;
    resolver->info = info;

    resolver->peers = 0;
    resolver->peerCount = 0;

    resolver->meshes = 0;
    resolver->meshCount = 0;
}

void CreateResolver( struct Peer* hostingPeer, struct Resolver* resolver )
{
    VALIDATE_OBJECT( resolver->peers = realloc( resolver->peers, sizeof(struct Resolver) * (resolver->peerCount+1) ), "CreateResolver - " );
    resolver->peers[resolver->peerCount++] = *hostingPeer;
}

void AddMeshToResolver( const char* meshId, struct Resolver* resolver )
{
    VALIDATE_OBJECT( resolver->meshes = realloc( resolver->meshes, sizeof(struct Mesh) * (resolver->meshCount+1) ), "AddMeshToResolver - " );
    InitializeAndCreateMesh( meshId, &resolver->meshes[resolver->meshCount++] );
}


// --------------------------------- PEER -------------------------------------- 
void InitializePeer( const char* name, const char* info, struct Peer* peer )
{
    LOG_CHECKPOINT("InitializePeer");

    peer->name = name;
    peer->info = info;

    CreateMessageQueue( MAX_MESSAGE_QUEUE_SIZE, &peer->sendQueue );

    peer->currentSendingByte = -1;
    peer->currentReceivingByte = 0;

    /*peer->resolvers = 0;
    peer->resolverCount = 0;*/

    peer->connectedPeers = 0;
    peer->connectedPeerCount = 0;

    InitializeSocket( name, info, &peer->connectedSocket ); // TODO: Better names for peer sockets... DO I want socket name/info same as peer name/info. Is there useful distinction can make? perhaps sprintf peerName+"-Socket"?
}

// TODO: Host may not be needed anymore since we are LISTENING, not necessarily connecting?
void CreatePeer( const char* host, const char* port, enum SocketType socketType, enum SocketProtocol socketProtocol, enum AddressFamily addressFamily, int flags, 
                 struct Peer* peer )
{
    LOG_CHECKPOINT("CreatePeer");

    struct AddressInfo* addressInfo;
    CreateSocket( host, port, socketType, socketProtocol, addressFamily, flags, &addressInfo, &peer->connectedSocket );

    if( host )
    {
        if( ConnectToAddress( &peer->connectedSocket, addressInfo ) )
        {
            char peer_ipv4_string[INET_ADDRESS_STRING_LENGTH];
            char peer_port_string[INET_PORT_STRING_LENGTH];
            LOG_ERROR("CreatePeer - Failed to connect to socket %s:%s (descriptor = %d) for the host peer of '%s'", 
                      InternetNetworkAddressToString( addressInfo->address->family, addressInfo->address->internetAddress, peer_ipv4_string, INET_ADDRESS_STRING_LENGTH ), 
                      InternetNetworkPortToString( addressInfo->address->port, peer_port_string, INET_PORT_STRING_LENGTH ), (int) peer->connectedSocket.descriptor, peer->name);
        }
    }
    else
        SocketBind( &peer->connectedSocket, addressInfo );
}

void CreatePeerFromConnectedSocket( struct Peer* peer, struct Socket* socket, const char* peerName, const char* peerInfo )
{
    LOG_CHECKPOINT("CreatePeerFromConnectedSocket");

    InitializePeer( peerName, peerInfo, peer );

    CreateSocketFromDescriptor( &peer->connectedSocket, socket->descriptor, socket->name, socket->info, &socket->addressInfo );
}

void ConnectPeerToNewPeer( struct Peer* peer, const char* host, const char* port, enum SocketType socketType, 
                           enum SocketProtocol socketProtocol, enum AddressFamily addressFamily, int flags )
{
    LOG_CHECKPOINT("ConnectPeerToNewPeer");

    struct Socket socket;
    struct AddressInfo* addressInfo;

    if( CreateSocket( host, port, socketType, socketProtocol, addressFamily, flags, &addressInfo, &socket ) )
    {
        LOG_ERROR("ConnectPeerToNewPeer - Failed to create socket for host: '%s' and port '%s'", host, port);
    }
    else
    {
        if( ConnectToAddress( &socket, addressInfo ) )
        {
            char peer_ipv4_string[INET_ADDRESS_STRING_LENGTH];
            char peer_port_string[INET_PORT_STRING_LENGTH];
            LOG_ERROR("ConnectPeerToNewPeer - Failed to connect to socket %s:%s (descriptor = %d) for the host peer of '%s'", 
                      InternetNetworkAddressToString( addressInfo->address->family, addressInfo->address->internetAddress, peer_ipv4_string, INET_ADDRESS_STRING_LENGTH ), 
                      InternetNetworkPortToString( addressInfo->address->port, peer_port_string, INET_PORT_STRING_LENGTH ), (int) peer->connectedSocket.descriptor, peer->name);
        }
        else
            AddPeerFromConnectedSocketToPeer( socket, peer, "INITIATING CONNECTION" );
    }
}

char* PeerGetIPV4Address( struct Peer* peer ) // TODO: Currently won't work for own host (i.e. can only use this function for connected (i.e. external) sockets) -> NEED MODIFY CREATE PEER (i.e. NOT InitializePeerFromSocket)
{
    static char address[INET_ADDRESS_STRING_LENGTH + 10]; // Space for Address:Port -> Static so can return char* (will reuse same memory every time call this function)
    char peer_ipv4_string[INET_ADDRESS_STRING_LENGTH];
    char peer_port_string[INET_PORT_STRING_LENGTH];
    InternetNetworkAddressToString( peer->connectedSocket.addressInfo.family, peer->connectedSocket.addressInfo.internetAddress, peer_ipv4_string, INET_ADDRESS_STRING_LENGTH );
    InternetNetworkPortToString( peer->connectedSocket.addressInfo.port, peer_port_string, INET_PORT_STRING_LENGTH );
    sprintf( address, "%s:%s", peer_ipv4_string, peer_port_string );

    return address;
}

void PeerSwap( struct Peer* peer1, struct Peer* peer2 )
{
    struct Peer tempPeer = *peer1;
    *peer1 = *peer2;
    *peer2 = tempPeer;
}

void AddPeerFromConnectedSocketToPeer( struct Socket socket, struct Peer* peer, const char* welcomeMessage )
{
    LOG_CHECKPOINT("AddPeerFromConnectedSocketToPeer");

    char peer_name[MAX_NAME_LENGTH];
    sprintf( peer_name, "Peer-%i", peer->connectedPeerCount );
    char peer_info[MAX_INFO_LENGTH];
    sprintf( peer_info, "Peer-%i recognized by peer '%s'", peer->connectedPeerCount, peer->name );

    VALIDATE_OBJECT( peer->connectedPeers = realloc(peer->connectedPeers, sizeof(struct Peer) * (peer->connectedPeerCount+1)), "AddPeerFromConnectedSocketToPeer - " );
    CreatePeerFromConnectedSocket( &peer->connectedPeers[peer->connectedPeerCount++], &socket, peer_name, peer_info );

    CreateAndEnqueueMessage( peer->name, strlen(peer->name), welcomeMessage, strlen(welcomeMessage), &peer->connectedPeers[peer->connectedPeerCount-1].sendQueue );
    SendMessageToPeer( &peer->connectedPeers[peer->connectedPeerCount-1] );
}

void RemoveConnectedPeerFromPeer( struct Peer* connectedPeer, struct Peer* peer )
{
    LOG_CHECKPOINT("RemoveConnectedPeerFromPeer");

    SOCKET socketToRemove = connectedPeer->connectedSocket.descriptor;

    for( int connectedPeerCounter = 0; connectedPeerCounter < peer->connectedPeerCount; ++connectedPeerCounter ) // TODO: Optimize with proper search algorithm (e.g. sort all clients by SOCKET # when add?)
                                                                                        //       Also, consider not doing dynamic memory...? Therefore removing would just be setting socket descriptor == NO_SOCKET
    {
        if( peer->connectedPeers[connectedPeerCounter].connectedSocket.descriptor == socketToRemove )
        {
            PeerSwap( &peer->connectedPeers[connectedPeerCounter], &peer->connectedPeers[--peer->connectedPeerCount] );
            DestroyPeer( &peer->connectedPeers[peer->connectedPeerCount] );

            //if( peer->connectedPeerCount )
            // TODO: Error check this, I noticed that I get WARN on SocketReceive for "lost connection" WSAECONNREST... Not sure if related to below (1 peer - 1 peer, i.e. don't need more than 2 executions)
            {
                VALIDATE_OBJECT( peer->connectedPeers = realloc(peer->connectedPeers, sizeof(struct Peer) * (peer->connectedPeerCount)), "RemoveConnectedPeerFromPeer - " );
            }
            /*else
            {
                FREE_OBJECT( peer->connectedPeers );
            }*/

            break;
        }
    }
}

void DefaultReceiveMessageHandler( struct Message* message )
{
    LOG_CHECKPOINT("DefaultReceiveMessageHandler");

    LOG_INFO("Received message from '%s': %s\n", message->sender, message->data);
}

int ReceiveMessageFromPeer( struct Peer* peer, ReceiveMessageHandler messageHandler )
{
    LOG_CHECKPOINT("ReceiveMessageFromPeer");

    LOG_DEBUG("Ready to recv() from %s", PeerGetIPV4Address(peer));

    int length_to_receive, current_bytes_received, total_bytes_received = 0;
    do
    {
        // Has the peer received a full Message yet? (struct Message being what is sent and received)
        if( peer->currentReceivingByte >= sizeof(peer->receivingMessage) )
        {
            messageHandler( &peer->receivingMessage );
            peer->currentReceivingByte = 0;
        }
        
        length_to_receive = sizeof(peer->receivingMessage) - peer->currentReceivingByte; // How much we can receive MINUS where we are currently
        current_bytes_received = SocketReceive( (char*)&peer->receivingMessage + peer->currentReceivingByte, length_to_receive, 0, &peer->connectedSocket );
        if( current_bytes_received > 0 )
        {
            peer->currentReceivingByte += current_bytes_received;
            total_bytes_received += current_bytes_received;
        }
        else if( current_bytes_received == 0 )
        {
            LOG_INFO("Connection closed");

            // TODO: Adding without direct testing, consider if needed. Idea: Want to handle any message left unhandled (this may be a case that occurs)
            if( peer->currentReceivingByte < sizeof(peer->receivingMessage) && peer->currentReceivingByte > 0 )
            {
                messageHandler( &peer->receivingMessage );
                peer->currentReceivingByte = 0;
            }

            return -1;
        }
        else if( current_bytes_received != -2 ) // bytes_received == SOCKET_ERROR, TODO: normally but currently doing workaround for "WouldBlock" error
        {
            LOG_ERROR("ReceiveMessageFromPeer - Failed when receiving data!");
            LOG_ERROR("WSA Last Error: %i\n", WSAGetLastError()); // TODO: Find better place for this
            // TODO: Teardown...?
        }

    } while( current_bytes_received > 0 );

    /*if( peer->currentReceivingByte <= sizeof(peer->receivingMessage) && peer->currentReceivingByte > 0 )
    {
        messageHandler( &peer->receivingMessage );
        peer->currentReceivingByte = 0;
    }*/

    return 0;
}

int SendMessageToPeer( struct Peer* peer )
{
    LOG_CHECKPOINT("SendMessageToPeer");

    LOG_DEBUG("Ready to send() to %s", PeerGetIPV4Address(peer));

    int length_to_send, current_bytes_sent, total_bytes_sent = 0;
    do {

        if( peer->currentSendingByte < 0 || peer->currentSendingByte >= sizeof(peer->sendingMessage) )
        {
            LOG_DEBUG("Nothing left to send for a particular message, checking queue if there is another message available");
            if( DequeueMessage(&peer->sendQueue, &peer->sendingMessage) != 0 )
            {
                LOG_DEBUG("No messages in queue to send");
                break;
            }
            LOG_DEBUG("Found message in queue!");
            //printf("\nSENDER: %s\n\n", peer->sendingMessage.sender);
            //printf("\nDATA: %s\n\n", peer->sendingMessage.data);
            peer->currentSendingByte = 0; // Start by sending the first byte (:
        }
        
        length_to_send = sizeof(peer->sendingMessage) - peer->currentSendingByte; // How much we can send - where we are currently
        if( length_to_send > MAX_SEND_SIZE )
            length_to_send = MAX_SEND_SIZE;
        current_bytes_sent = SocketSend( (char*)&peer->sendingMessage + peer->currentSendingByte, length_to_send, 0, &peer->connectedSocket );
        if( current_bytes_sent > 0 )
        {
            peer->currentSendingByte += current_bytes_sent;
            total_bytes_sent += current_bytes_sent;
        }
        else if( current_bytes_sent == 0 )
        {
            LOG_DEBUG("Peer doesn't seem able to accept data. Try again later");
            break;
        }
        else // bytes_sent == SOCKET_ERROR
        {
            LOG_ERROR("SendMessageToPeer - Failed when sending data!");
            LOG_ERROR("WSA Last Error: %i\n", WSAGetLastError()); // TODO: Find better place for this
            // TODO: Teardown...?
        }

    } while( current_bytes_sent > 0 );

    LOG_DEBUG("Total Bytes Sent: %i", total_bytes_sent);
    return 0;
}


// Convenience function for sending (consistent with my other Networking API)
void PeerSendToPeer( struct Peer* fromPeer, const char* sendBuffer, int flags, struct Peer* toPeer ) // TODO: Flags may contain broadcast and what not? But then whats point of toPeer? NULL in this case?
{
    LOG_CHECKPOINT("PeerSendToPeer");

    CreateAndEnqueueMessage( fromPeer->name, strlen(fromPeer->name), sendBuffer, strlen(sendBuffer), &toPeer->sendQueue ); // TODO: Logic here may be backwards, but ultimately want to say "I have a message to send to this peer
                                                                                                                          //          so put a message in it's sendQueue so that when handling I know there is a message to send to it"??
                                                                                                                          //          THINK about?
    SendMessageToPeer( toPeer );
}

void PeerSendToPeers( struct Peer* fromPeer, const char* sendBuffer, int flags, struct Peer* toPeers, int toPeerCount )
{
    LOG_CHECKPOINT("PeerSendToPeers");

    if( !toPeerCount )
        toPeerCount = fromPeer->connectedPeerCount;

    for( int peerCounter = 0; peerCounter < toPeerCount; ++peerCounter )
    {
        if( toPeers )
            PeerSendToPeer( fromPeer, sendBuffer, 0, &toPeers[peerCounter] );
        else
            PeerSendToPeer( fromPeer, sendBuffer, 0, &fromPeer->connectedPeers[peerCounter] );
    }
}

void PeerListen( struct Peer* peer )
{
    LOG_CHECKPOINT("PeerListen");

    // TODO: below is new, from old server code where startserver caused listenning and then loopy loop
    SocketListen( &peer->connectedSocket, WAIT_QUEUE_MAX_SIZE );
}

void PeerHandleNewConnection( struct Peer* peer )
{
    LOG_CHECKPOINT("PeerHandleNewConnection");

    AddPeerFromConnectedSocketToPeer( SocketAccept( &peer->connectedSocket ), peer, "RECEIVING CONNECTION" );
}

void BuildPeerFileDescriptorSets( struct Peer* peer, fd_set* readFDs, fd_set* writeFDs, fd_set* exceptFDs )
{
    FD_ZERO( readFDs );
    FD_SET( peer->connectedSocket.descriptor, readFDs );

    for( int connectedPeerCounter = 0; connectedPeerCounter < peer->connectedPeerCount; ++connectedPeerCounter ) // TODO: later refactor with MAX_PEER? and remove dynamic client amount (for time opti)
    {
        FD_SET( peer->connectedPeers[connectedPeerCounter].connectedSocket.descriptor, readFDs );
    }


    FD_ZERO( writeFDs );
    for( int connectedPeerCounter = 0; connectedPeerCounter < peer->connectedPeerCount; ++connectedPeerCounter )
    {
        // If PEER has data to send to this particular peer (i.e. "I have bytes that I am currently sending to peer")
        if( peer->connectedPeers[connectedPeerCounter].currentSendingByte > 0 )
            FD_SET( peer->connectedPeers[connectedPeerCounter].connectedSocket.descriptor, writeFDs );
    }

    FD_ZERO( exceptFDs );
    for( int connectedPeerCounter = 0; connectedPeerCounter < peer->connectedPeerCount; ++connectedPeerCounter )
    {
        FD_SET( peer->connectedPeers[connectedPeerCounter].connectedSocket.descriptor, exceptFDs );
    }
}

void CustomHandlePeerEvents( HandlePeerEventsHandler handlePeerEventsHandler, struct Peer* peer )
{
    LOG_CHECKPOINT("CustomHandlePeerEvents");

    (*handlePeerEventsHandler)(peer);
}

void HandlePeerEvents( struct Peer* peer )
{
    fd_set read_fds;
    fd_set write_fds;
    fd_set except_fds;

    int count;
    {
        BuildPeerFileDescriptorSets( peer, &read_fds, &write_fds, &except_fds );

        count = SocketStatus( 0, &read_fds, &write_fds, &except_fds );
        if( count > 0 )
        {
            if( FD_ISSET( peer->connectedSocket.descriptor, &read_fds ) )
            {
                PeerHandleNewConnection( peer );
            }

            for( int connectedPeerCounter = 0; connectedPeerCounter < peer->connectedPeerCount; ++connectedPeerCounter )
            {
                if( FD_ISSET( peer->connectedPeers[connectedPeerCounter].connectedSocket.descriptor, &read_fds ) )
                {
                    if( ReceiveMessageFromPeer( &peer->connectedPeers[connectedPeerCounter], &DefaultReceiveMessageHandler ) != 0 )
                    {
                        RemoveConnectedPeerFromPeer( &peer->connectedPeers[connectedPeerCounter], peer );
                        --connectedPeerCounter;
                        continue;
                    }
                    //LOG_INFO("Received: %s", peer->connectedPeers[connectedPeerCounter].receivingMessage.data);
                }

                if( FD_ISSET( peer->connectedPeers[connectedPeerCounter].connectedSocket.descriptor, &write_fds ) )
                {
                    // TODO: Expand current SendMessageToPeer functionality (which is WELCOME after connect), to be able to send messages back and forth (text in console?)
                    if( SendMessageToPeer( &peer->connectedPeers[connectedPeerCounter] ) != 0 )
                    {
                        LOG_DEBUG("SEND MESSAGE HAD 0 RETURN");
                        //TODO: CloseClientConnection(...)
                        continue;
                    }
                }

                if( FD_ISSET( peer->connectedPeers[connectedPeerCounter].connectedSocket.descriptor, &except_fds ) )
                {
                    LOG_ERROR("HandlePeerEvents - Exception for peer '%s'", peer->connectedPeers[connectedPeerCounter].name);
                    RemoveConnectedPeerFromPeer( &peer->connectedPeers[connectedPeerCounter], peer );
                    continue;
                }
            }

            if( FD_ISSET( peer->connectedSocket.descriptor, &except_fds ) )
            {
                LOG_ERROR("HandlePeerEvents - Exception for peer '%s'", peer->name);
                DestroyPeer( peer );
            }

        }
        else if( count == SOCKET_ERROR ) // SOCKET_ERROR = -1
        {
            LOG_ERROR("HandlePeerEvents - Failed during updating socket sets");
            LOG_ERROR("WSA Last Error: %i\n", WSAGetLastError()); // TODO: Find better place for this, or a better wrapper so can be used generally... ("Last Error: %i", GetLastError())?
        } // else if( count == 0 ) -> Case of a timeout from select()
    }
}

void DestroyPeer( struct Peer* peer )
{
    LOG_CHECKPOINT("DestroyPeer");

    // Destroy peer (especially socket) for all that this host has connected
    for( int connectedPeerCounter = 0; connectedPeerCounter < peer->connectedPeerCount; ++connectedPeerCounter )
    {
        DestroyPeer( &peer->connectedPeers[connectedPeerCounter] );
    }

    if( peer->connectedPeers )
    {
        FREE_OBJECT( peer->connectedPeers );
        peer->connectedPeers = 0;
    }
    peer->connectedPeerCount = 0;

    DestroyMessageQueue( &peer->sendQueue );
    DestroySocket( &peer->connectedSocket );

    peer->currentSendingByte = -1;
    peer->currentReceivingByte = 0;
}








