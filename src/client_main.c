

#include <winsock2.h> // Includes Windows.h already, if needed though, precede with macro: #define WIN32_LEAN_AND_MEAN
#include <ws2tcpip.h>

#include <stdio.h>

#ifndef FULL_LOGGING
#define FULL_LOGGING
#endif
#include "common.h"

#include "socket.h"
#include "client.h"

int main()
{
    CREATE_FULL_LOGGING(llPRESET_PARANOID, lePRESET_VERY_GRANULAR, lvPRESET_ALL);

    struct Client client;
    InitializeClient( "Client", "Is served up", &client );
    char port[6];
    printf("Enter in port number to connect to: "); scanf("%s", port);
    CreateClient( "localhost", port, stSTREAM, spIPPROTOCOL_TCP, afINET, aifPASSIVE, &client );

    char receiveBuffer[100];
    char myChar = 'y';
    fflush(stdin);
    do
    {
        if( myChar == 's' ) // Send message
        {
            char message[100];
            printf("Enter in message!: "); fflush(stdin); fgets(message, 100, stdin); fflush(stdin);
            message[strlen(message)] = '\0';
            
            ClientSend( message, strlen(message), 0, &client );
        }
        ClientReceive( receiveBuffer, 100, 0, &client );

    } while( (myChar = getchar()) != 'n');

    DestroyClient( &client );

    DESTROY_FULL_LOGGING();
    return 0;
}

